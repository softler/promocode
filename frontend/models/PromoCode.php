<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "promo_code".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $sum
 * @property integer $status
 * @property integer $created_at
 * @property integer $update_at
 */
class PromoCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'sum', 'status', 'created_at', 'update_at'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city_id' => 'City ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'sum' => 'Sum',
            'status' => 'Status',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }
}
