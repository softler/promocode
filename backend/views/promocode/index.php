<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promo Codes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-code-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Promo Code', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'start_date',
            'end_date',
            'sum',
            'name',
            [
                'attribute'=>'city_id',
                'value'=>function ($model, $key, $index, $column) {
                    return $model['city']['city_name'];
                },
            ],

            // 'created_at',
            // 'update_at',
            [
                'attribute'=>'status',
                'value'=>function ($model, $key, $index, $column) {
                    return $model['status']==0?'Inactive':'Active';
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => ' {update} ',
                'buttons'=>[
                    'update' => function ($url, $model, $key) {
                        return $model->status == 1 ? Html::a('Update', $url) : '';
                    },
                ],
            ],
        ],
    ]); ?>
</div>
