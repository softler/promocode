<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\models\PromoCode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'start_date')->widget(\yii\jui\DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd'
    ]) ?>

    <?= $form->field($model, 'end_date')->widget(\yii\jui\DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd'
    ]) ?>
    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(\backend\models\City::find()->all(), 'id', 'city_name'));?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
