<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "promo_code".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $sum
 * @property integer $status
 * @property integer $created_at
 * @property integer $update_at
 */
class PromoCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'status', 'created_at', 'update_at'], 'integer'],
            ['sum', 'integer', 'min'=>1],
            [['city_id', 'name', 'city', 'sum','start_date', 'end_date'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            ['start_date','validateDates'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city_id' => 'City',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'sum' => 'Sum',
            'status' => 'Status',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }

    public function validateDates(){
        if(strtotime($this->end_date) <= strtotime($this->start_date)){
            $this->addError('start_date','Please give correct Start and End dates');
            $this->addError('end_date','Please give correct Start and End dates');
        }
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
