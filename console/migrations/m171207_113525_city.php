<?php

use yii\db\Migration;

/**
 * Class m171207_113525_city
 */
class m171207_113525_city extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(11),
            'city_name' => $this->string(64),
            'created_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
        $this->insert('city',['city_name' => 'Yerevan']);
        $this->insert('city',['city_name' => 'Moscow']);
        $this->insert('city',['city_name' => 'New-York']);
        $this->insert('city',['city_name' => 'Tbilisi']);
        $this->insert('city',['city_name' => 'Odesa']);
        $this->insert('city',['city_name' => 'Venecia']);
        $this->insert('city',['city_name' => 'Dubai']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return $this->dropTable('{{%city}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_113525_city cannot be reverted.\n";

        return false;
    }
    */
}
