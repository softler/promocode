<?php

use yii\db\Migration;

/**
 * Class m171207_112500_promo_code
 */
class m171207_112500_promo_code extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%promo_code}}', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(64)->unique(),
            'city_id' => $this->integer(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'sum' => $this->integer()->unsigned(),
            'status' => $this->integer(),
            'created_at' => $this->integer()->Null(),
            'update_at' => $this->integer()->Null(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%promo_code}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_112500_promo_code cannot be reverted.\n";

        return false;
    }
    */
}
