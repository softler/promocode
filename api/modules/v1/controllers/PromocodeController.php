<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use backend\models\PromoCode;
use backend\models\City;

/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class PromocodeController extends ActiveController
{
    public $modelClass = 'backend\models\PromoCode';


    protected function verbs()
    {
        return [
            'get-discount-info' => ['GET'],
            'get-activate-discount' => ['GET'],
        ];
    }


    /**
     * Get discount info
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     */
    public function actionGetDiscountInfo($name)
    {
        $result = PromoCode::find()->where(['name' => $name])->one();
        if ($result) {
            return ['success' => true, 'data' => $result];
        } else {
            return ['success' => false, 'message' => "This Promocode is not found"];
        }
    }

    /**
     * Get activate discount
     * @param $name
     * @param $city_name
     * @return array
     */
    public function actionGetActivateDiscount($name, $city_name)
    {
        $city = City::find()->where(['city_name' => $city_name])->one();
        $city_id = $city->id;
        $resultPromocode = PromoCode::find()->where(['name' => $name, 'city_id' => $city_id])->one();
        if ($resultPromocode) {
            $sum = $resultPromocode->sum;
            $resultPromocode->status = 0;
            $resultPromocode->save();
            return ['success' => true, 'data' => $sum];
        } else {
            return ['success' => false, 'message' => "Promocode is not found"];
        }

    }
}